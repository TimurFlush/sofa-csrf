<?php

namespace TimurFlush\SofaCsrf;

use Phalcon\Session\AdapterInterface;
use Phalcon\Security\Random;

/**
 * Class Protection
 * @package TimurFlush\SofaCsrf
 */
class Protection
{
    /**
     * @var \Phalcon\Session\AdapterInterface
     */
    private $_session;

    /**
     * @var string
     */
    private $_prefix;

    /**
     * Protection constructor.
     * @param AdapterInterface $adapter
     * @param string $prefix
     */
    public function __construct(AdapterInterface $adapter, string $prefix = '')
    {
        $this->_session = $adapter;
        $this->_prefix = $prefix;

        if (!$this->tokenExists())
            $this->generateToken();
    }

    /**
     * @param string $prefix
     */
    public function generateToken(string $prefix = null) : void
    {
        $prefix = $prefix ?? $this->_prefix;

        $random = new Random();

        $this->_session->set(__CLASS__ . $prefix, [
            'tokenKey' => 'TF' . $random->hex(30),
            'tokenValue' => 'TF' . $random->hex(30)
        ]);
    }

    /**
     * @param string $prefix
     * @param string $tokenKey
     * @param string $tokenValue
     * @return bool
     */
    public function checkToken(string $tokenKey, string $tokenValue, string $prefix = null) : bool
    {
        $prefix = $prefix ?? $this->_prefix;
        if ($this->_session->has(__CLASS__ . $prefix)) {
            $token = $this->_session->get(__CLASS__ . $prefix);
            if ($token['tokenKey'] == $tokenKey && $token['tokenValue'] == $tokenValue) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param string $prefix
     * @return bool
     */
    public function tokenExists(string $prefix = null) : bool
    {
        $prefix = $prefix ?? $this->_prefix;

        if ($this->_session->has(__CLASS__ . $prefix))
            return true;

        return false;
    }

    /**
     * @param string $prefix
     * @return array|bool
     */
    public function getToken(string $prefix = null)
    {
        $prefix = $prefix ?? $this->_prefix;

        if ($this->_session->has(__CLASS__ . $prefix)) {
            $token = $this->_session->get(__CLASS__ . $prefix);
            return [
                'tokenKey' => $token['tokenKey'],
                'tokenValue' => $token['tokenValue']
            ];
        }
        return false;
    }
    
    /**
     * @return string
     */
    public function html() : string
    {
        $token = $this->getToken();
        return "<input type=\"hidden\" name=\"{$token['tokenKey']}\" value=\"{$token['tokenValue']}\" />";
    }
}